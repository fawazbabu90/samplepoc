//
//  Constants.swift
//  SamplePOC
//
//  Created by Fawaz on 08/07/18.
//  Copyright © 2018 Fawaz. All rights reserved.
//

import Foundation
import UIKit

let navigationTitle = "List View"
let urlString : String = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"

//MARK: - Constants related to table view
let reuseIdentifier: String = "reuseIdentifier"
let defaultTitleString : String = "No title available for this row"
let defaultDescription : String = "No decsription available for this row"
let defaultMessage: String = "Click on Refresh icon or \"Pull to Refresh\" to fetch the data from server."

//MARK: - Constants related to network
let alertMessage: String = "You are not connected to any active network"
let okText: String = "OK"

//MARK: - 
typealias DownloadComplete = () -> ()
