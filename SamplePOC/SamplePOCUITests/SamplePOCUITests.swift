//
//  SamplePOCUITests.swift
//  SamplePOCUITests
//
//  Created by Fawaz on 08/07/18.
//  Copyright © 2018 Fawaz. All rights reserved.
//

import XCTest

class SamplePOCUITests: XCTestCase {
        
  override func setUp() {
    super.setUp()
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
      continueAfterFailure = false
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    XCUIApplication().launch()

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testExample() {
    // Use recording to get started writing UI tests.
    // Use XCTAssert and related functions to verify your tests produce the correct results.

  }
  
  /**
   This test to check the pull to refresh of table view.
   */
  func testPullToRefresh(){
    XCUIApplication().tables.staticTexts["Click on Refresh icon or \"Pull to Refresh\" to fetch the data from server."].swipeDown()
  }
  
  /**
   This test to check the tap of refresh button
   */
  func testRefreshButton(){
    XCUIApplication().navigationBars["About Canada"].buttons["Refresh"].tap()
  }
  
  /**
   To pass this test Wifi needs to switched off and if in mobile it should be in aeroplane mode.
   */
  func testNoNetworkPopUpAction(){
    let app = XCUIApplication()
    app.alerts["Alert"].buttons["OK"].tap()
    app.tables/*@START_MENU_TOKEN@*/.staticTexts["Click on Refresh icon or \"Pull to Refresh\" to fetch the data from server."].swipeLeft()/*[[".cells.staticTexts[\"Click on Refresh icon or \\\"Pull to Refresh\\\" to fetch the data from server.\"]",".swipeDown()",".swipeLeft()",".staticTexts[\"Click on Refresh icon or \\\"Pull to Refresh\\\" to fetch the data from server.\"]"],[[[-1,3,1],[-1,0,1]],[[-1,2],[-1,1]]],[0,0]]@END_MENU_TOKEN@*/
  }
  
}
