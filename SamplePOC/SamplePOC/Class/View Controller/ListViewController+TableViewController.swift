//
//  ListViewController+TableViewController.swift
//  SamplePOC
//
//  Created by Fawaz on 09/07/18.
//  Copyright © 2018 Fawaz. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage


extension ListViewController:UITableViewDelegate,UITableViewDataSource{
  
  // MARK: - Table View Data source delegate methods
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {    
    if(viewModel.listData.count > 0){
      return viewModel.listData.count
    }
    else{
      return 1
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as! ListDetailsCell
  
    if(self.viewModel.listData.count>0){

      let obj: ListModelDetails = self.viewModel.listData[indexPath.row]
      
      // To display the title
      cell.listTitle.text = getTitleName(index: indexPath.row)
      
      // To display the description
      cell.listDescription.text = getDecriptionText(index: indexPath.row)
        
      // To display the image
      if let imageUrl = obj.imageHref, obj.imageHref != nil{
        let imgUrl = URL(string: imageUrl)
        cell.imgView.sd_setImage(with: imgUrl, completed: { image, error, cacheType, imageURL in
          if ((image) != nil){
            cell.imgView.image = image
          }
          else{
            cell.imgView.image = UIImage(named: "No_Image")
          }
          cell.layoutSubviews()
          cell.setNeedsLayout()
        })
      }
      else{
        cell.imgView.image = UIImage(named: "No_Image")
      }
    }
    else{
      cell.listDescription.text = defaultMessage
    }

    print(cell.contentView.frame.width)
    cell.contentView.clipsToBounds = true
    return cell
    
  }
  
  // MARK: - User defined method
  
  /**
   This method returns the title name from the array of ListModel class object. If the title is empty, it returns a default title
   - parameter: index - index of the object.
   - returns: String: Title text will be returned back
   */
  func getTitleName(index: Int) -> String{
    let obj: ListModelDetails = self.viewModel.listData[index]
    if let titleText = obj.title, obj.title != nil {
      return titleText
    }
    else{
      return defaultTitleString
    }
  }
  
  /**
   This method returns the title's description from the array of ListModel class object. If the title's description is empty, it returns a default decscription.
   - parameter: index - index of the object.
   - returns: String: Title text will be returned back
   */
  func getDecriptionText(index: Int) -> String{
    let obj: ListModelDetails = self.viewModel.listData[index]
    if let descriptionText = obj.description, obj.description != nil {
     return descriptionText
    }
    else{
      return defaultDescription
    }
  }
}
