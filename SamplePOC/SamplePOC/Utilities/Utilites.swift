//
//  Utilies.swift
//  SamplePOC
//
//  Created by Fawaz on 10/07/18.
//  Copyright © 2018 Infosys. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

/**
 This class is for utilities. Performing miscellaneous actions.
 */
class Utilies: NSObject {
  
  class func showProgressBar(){
    SVProgressHUD .show()
    SVProgressHUD .setDefaultStyle(SVProgressHUDStyle.custom)
    SVProgressHUD .setRingThickness(1.0)
  }
  
  class func dismissProgressBar(){
    SVProgressHUD .dismiss()
  }
  
}

/**
 This class is for connectivity check.
 */
class Connectivity {
  
  class func isConnectedToInternet() ->Bool {
    return NetworkReachabilityManager()!.isReachable
  }
}
