//
//  ListViewController.swift
//  SamplePOC
//
//  Created by Fawaz on 08/07/18.
//  Copyright © 2018 Fawaz. All rights reserved.
//

import UIKit
import Masonry

class ListViewController: UIViewController {

  var myTableView: UITableView!
  private var refreshControler: UIRefreshControl!
  
  var spWebServiceHandler: SPWebService!
  
  lazy var viewModel: ListViewModel = {
    return ListViewModel()
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.setupUI()

  }

  override func viewDidAppear(_ animated: Bool) {
    if(Connectivity.isConnectedToInternet()){
      self.fetchDataFromServer()
    }else {
      self .showAlertView()
    }
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  /**
   This method is to setup the basic UI.
   
   - parameter nil.
   - returns: nil.
   */
  func setupUI(){
    
    self.navigationItem.title = navigationTitle
    
    // Set the navigation bar style as default
    UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    
    // Creates right bar button and its action
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.refresh, target: self, action:#selector(buttonRefreshClicked))
    
    //Invoke method to set up table view
    setUpTableView()
    
  }
  
  /**
   This method sets up the table view properties, data source, delegates and constraints.
   
   - parameter: nil.
   - returns: nil.
   */
  func setUpTableView(){
    // Create the tableview
    myTableView = UITableView()
    self.view.addSubview(myTableView)
    myTableView.mas_makeConstraints({ (make:MASConstraintMaker?) in
      make?.edges.equalTo()(self.view)
    })
    myTableView.register(ListDetailsCell.self, forCellReuseIdentifier: reuseIdentifier)
    myTableView.dataSource = self
    myTableView.delegate = self
    myTableView.rowHeight = UITableViewAutomaticDimension
    myTableView.estimatedRowHeight = 70.0
    
    setUpPullToRefresh()
  }
  
  /**
   This method is to sets up the pull to refresh functionality of the table view.
   
   - parameter: nil.
   - returns: nil.
   */
  func setUpPullToRefresh(){
    // Tableview's pull to refresh control
    refreshControler = UIRefreshControl.init()
    if #available(iOS 10.0, *) {
      myTableView.refreshControl = refreshControler
    } else {
      myTableView.addSubview(refreshControler)
    }
    
    refreshControler.addTarget(self, action: #selector(pullToRefresh), for: UIControlEvents.valueChanged)
  }
  
  /**
   This method is to fetch tha data from the server.
   
   - parameter: nil.
   - returns: nil.
   */
  func fetchDataFromServer(){
    viewModel.reloadTableViewClosure = { [weak self] () in
      DispatchQueue.main.async {
        self?.refreshControler.endRefreshing()
        if let _title = self?.viewModel.title, self?.viewModel.title != nil{
          self?.navigationItem.title = _title
        }
        self?.myTableView.reloadData()
      }
    }
    Utilies.showProgressBar()
    viewModel.initFetch()
  }
  
  /**
   This method refreshes the data on refresh button click.
   
   - parameter sender: Any parameter can be paased.
   - returns: nil.
   */
  
  @IBAction func buttonRefreshClicked(_ sender: Any) {
    if(Connectivity.isConnectedToInternet()){
      DispatchQueue.main.async (
        execute:{
          UIApplication.shared.isNetworkActivityIndicatorVisible = true
      }
      )
      refreshControler .endRefreshing()
      self.fetchDataFromServer()
    }
    else{
      self .showAlertView()
    }

  }
  
  /**
   This method refreshes the data by pulling down the table view.
   
   - parameter sender: Any parameter can be paased.
   - returns: nil.
   */
  
  @IBAction func pullToRefresh(_ sender: Any) {
    if(Connectivity.isConnectedToInternet()){
      refreshControler .beginRefreshing()
      DispatchQueue.main.async (
        execute:{
           UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
      )
      self.fetchDataFromServer()
    }
    else{
      self .showAlertView()
    }
  }
  
  /**
   This method show the alert view in case there is no active internet.
   
   - parameter: nil.
   - returns: nil.
   */
  
  func showAlertView(){
    let alert = UIAlertController(title: "", message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: okText, style: .default, handler: nil))
    self.present(alert, animated: true, completion: nil)
    if(refreshControler.isRefreshing){
      refreshControler .endRefreshing()
    }
  }
}
