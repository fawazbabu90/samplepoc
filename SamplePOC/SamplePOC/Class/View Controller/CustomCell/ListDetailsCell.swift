//
//  ListDetailsCell.swift
//  SamplePOC
//
//  Created by Fawaz on 14/07/18.
//  Copyright © 2018 Fawaz. All rights reserved.
//

import UIKit
import Masonry

class ListDetailsCell: UITableViewCell {

  let imgView = UIImageView()
  let listTitle = UILabel()
  let listDescription = UILabel()
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    listDescription.numberOfLines = 0
    
    contentView.addSubview(listTitle)
    contentView.addSubview(listDescription)
    contentView.addSubview(imgView)

    setUpImageView()
    setUpTitleView()
    setUpTitleDescpritionView()
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  /**
   This method sets up the constraints for the image view.
   */
  func setUpImageView(){
    imgView.mas_makeConstraints { (make:MASConstraintMaker?) in
      make?.top.equalTo()(contentView)?.with().offset()(10)
      make?.left.equalTo()(contentView)?.with().offset()(10)
      make?.width.equalTo()(60)
      make?.height.equalTo()(60)
    }
  }

  /**
   This method sets up the constraints for the title.
   */
  func setUpTitleView(){
    let width = contentView.frame.width * 0.8
    listTitle.mas_makeConstraints { (make:MASConstraintMaker?) in
      make?.top.equalTo()(contentView)?.with().offset()(10)
      make?.left.equalTo()(imgView.mas_right)?.with().offset()(10)
      make?.right.equalTo()(contentView)?.with().offset()(-10)
      make?.width.greaterThanOrEqualTo()(width)
    }
  }

  /**
   This method sets up the constraints for the description.
   */
  func setUpTitleDescpritionView(){
    listDescription.mas_makeConstraints { (make:MASConstraintMaker?) in
      make?.top.equalTo()(listTitle.mas_bottom)?.with().offset()(10)
      make?.left.equalTo()(imgView.mas_right)?.with().offset()(10)
      make?.bottom.equalTo()(contentView)?.with().offset()(-15)
      make?.right.equalTo()(contentView)?.with().offset()(-10)
    }
  }
}
