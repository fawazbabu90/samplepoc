//
//  SamplePOCTests.swift
//  SamplePOCTests
//
//  Created by Fawaz on 08/07/18.
//  Copyright © 2018 Fawaz. All rights reserved.
//

import XCTest
import UIKit

@testable import SamplePOC

class SamplePOCTests: XCTestCase {
  
  var listView : ListViewController!
  var spWebservice: SPWebService!
  
  override func setUp() {
    super.setUp()
   
    listView = ListViewController()
    _ = listView.view
    spWebservice = SPWebService()
   
  }
 
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
    listView = nil
  }
 
  func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measure {
      
    }
  }
  
  func testTableView(){
    XCTAssertNotNil(listView.myTableView)
  }
  
  func testThatViewConformsToUITableViewDataSource(){
    XCTAssertTrue(listView.myTableView.dataSource is ListViewController)
  }
  
  func testThatViewConformsToUITableViewDelegate(){
    XCTAssertTrue(listView.myTableView.delegate is ListViewController)
  }
  
  
  func test_fetch_data() {
    
    // Given A apiservice
    let sampleWS = self.spWebservice!
    
    let expect = XCTestExpectation(description: "callback")
    
    sampleWS.getFactsFromServer(complete: { (success, list, error) in
      expect.fulfill()
      XCTAssertEqual( list.listModelDetails?.count, 14)
    })
    
    wait(for: [expect], timeout: 5)
  }
}
