//
//  SPWebServiceHandler.swift
//  SamplePOC
//
//  Created by Fawaz on 08/07/18.
//  Copyright © 2018 Fawaz. All rights reserved.
//

import UIKit
import Alamofire

enum APIError: String, Error {
  case noNetwork = "No Network"
  case serverOverload = "Server is overloaded"
  case permissionDenied = "You don't have permission"
  case invalidUrl = "404 Error. URL not valid"
  case serverTimeOut = "Server Timed Out"
}

/**
 Protocols for delegating back to view model class.
 */
protocol SPWebServiceProtocol {
  func getFactsFromServer( complete: @escaping ( _ success: Bool, _ listData: ListModel, _ error: APIError? )->() )
}

class SPWebService: SPWebServiceProtocol {
  
  var _title = String()
  var _listData = [ListModelDetails]()
  
  var title: String {
    return _title
  }
  
  var listData: [ListModelDetails] {
    return _listData
  }
  
  /**
   This method fetch the data from the server .
   
   - returns: It returns the success status, ListModel object and error.
   */
  func getFactsFromServer(complete: @escaping ( _ success: Bool, _ listData: ListModel,  _ error: APIError? )->() ) {
    
    Alamofire.request(urlString).responseString{ response in
      if(response.response?.statusCode == 200){
        if let jsonString = response.result.value{
          if let data = (jsonString as String).data(using: String.Encoding.utf8) {
            do{
              let decoder = JSONDecoder()
              let list = try decoder.decode(ListModel.self, from: data)
              
              complete(true, list,  nil)
              
            } catch let err {
              print("Error", err.localizedDescription)
              let list = ListModel()
              complete(true,list, APIError(rawValue: err.localizedDescription))
            }
          }
        }
      }
      else {
        let list = ListModel()
        complete(true, list, APIError.serverTimeOut)
      }
    }
  }
}
