//
//  ListViewModel.swift
//  SamplePOC
//
//  Created by Fawaz on 08/07/18.
//  Copyright © 2018 Fawaz. All rights reserved.
//

import UIKit

class ListModel: Codable {
  var title: String?
  var listModelDetails: [ListModelDetails]?
  
  private enum CodingKeys: String, CodingKey{
    case title = "title"
    case listModelDetails = "rows"
  }
}

class ListModelDetails: Codable{
  
  var title: String?
  var description: String?
  var imageHref: String?
  
  private enum CodingKeys: String, CodingKey{
    case title = "title"
    case description = "description"
    case imageHref = "imageHref"
  }
}
