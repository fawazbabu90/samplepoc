//
//  ListViewModel.swift
//  SamplePOC
//
//  Created by Fawaz on 13/07/18.
//  Copyright © 2018 Fawaz. All rights reserved.
//

import UIKit
import Foundation

class ListViewModel: NSObject {
  
  var spWebService: SPWebServiceProtocol
  var title: String = " "
  var listData = [ListModelDetails]()
  
  init( _spWebService: SPWebServiceProtocol = SPWebService()) {
    self.spWebService = _spWebService
  }

  /**
   This method invokes the network function to fetch data
   
   parameter: nil
   returns: nil
   */
  func initFetch() {
    self.isLoading = true
    spWebService.getFactsFromServer{ [weak self] (sucess, list , error) in
      self?.isLoading = false
      Utilies.dismissProgressBar()
      if error == nil {
        self?.pocessFetchedData(list: list)
      }
    }
  }
  
  var reloadTableViewClosure: (()->())?
  
  var isLoading: Bool = false {
    didSet {
      self.reloadTableViewClosure?()
    }
  }
  
  /**
   This method process the fetched data to view model object
   
   parameter: ListModel - Object of class ListModel
   returns: nil
   */
  func pocessFetchedData(list: ListModel){
    if let _title = list.title, list.title != nil {
      self.title = _title
    }
    self.listData = [ListModelDetails]()
    if let factModel = list.listModelDetails {
      for(_, factDetails) in factModel.enumerated() {
        self.listData.append(factDetails)
      }
    }
  }
}

